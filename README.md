Don't donate if you struggle financially;
only if you have more than enough!
[ TL;DR ]: Help me with rent so I can create non-proprietary, public domain ideas & content(code, art, systems, etc) for you to use, play, remix, remake, and beyond without attribution/credit/worry.


📽 [Video]: Off-topic. It's me playing the game "Worlds Adrift"(not playable anymore). Just wanted to show off as a lost, favored game I'd love to play again(with you! No really, I'm serious - it was great to sail the skies with strangers and friends alike!).
💰 [Goal]:  3 years worth of my current bills. 3 years of sustenance and survival. The goal is not so important, I'm just leaving it visible for some semblance of measurement - I am both grateful for far less, and yet also I would very much like to live longer than 3 years : P )


Ahoy! Lucid dreams to you; farewell and well-fare whenever we part; and tell me your words of wisdom that you'd most wish to pass on, and the.
My name is Nohbdy Ahtall. I want to...
Help bring about permissively licensed, public domain creations of code, art, ideas, systems, and beyond.


...and, though I may be repeating myself here, I want to do it in a  way that is...
as an un-revenue model;
donation-only(PWYW);
open/libre software, hardware, and knowledge;
public domain content (creation);
inter-linking more people thinking like this;
rewarding+incentivizing those thinking like this;



The MD0 license I created is my first step towards this. It is built off of the CC0-1.0 license, and remains quite similar, yet attempts extra oomph towards anti-trademark and anti-patent aspects. Check this comparison of CC0-1.0 to MD0-0.7 here since it's every word of legalese that matters.
MD0 - as well as CC0, copyleft, leftcopy, and similar licenses - is a great example of what I'm all about, and where my intents and motivations are: free, long-lasting stuff for humankind. That's what your donations would be going toward. Not proprietary black-boxes, not trade secrets, not profit-oriented nonsense. Blueprints, knowledgebases, frameworks... things that can be infinitely replicated and distributed. These are powerful support structures for creators of physical/tangible things, too.


FAQ:: What's with the video?: Oh, it's just some gameplay from "Worlds Adrift". I am an internet/virtual bound person - we(yes you, reader - and I) are more likely to meet online than anywhere else. Be more fun, too. The game's gone now, but it's an inspiration - we can make it again.
# About::Nohbdy # 

Here's a bit of a word salad/cloud to describe me. Bold means they are especially so. A "[*]" symbol means this is not really what I think of myself perhaps, but likely what others would. I'm laying out my cons for your sake - no surprises I hope.

✅ Pros / Positives ✅

~ Unconventional ~ Innovative ~ Honest & Genuine ~ Game programmer wanna-be ~ Idea guy ~ Upbeat ~  Technocentric/Sustaincentric ~ Critical of status-quo, social norms ~ Anti-oligarchy, anti-corporatocracy, anti-servitude ~ Optimistic ~ Lover of humankind ~ Non-lethal/reverse-lethal advocate ~ Modularity and Reusability ~

 ⚠ Cons / Negatives ⚠

 ~ Lazy-ish ~ Non-committal ~ [*]Needs many/deep mental health breaks ~ [*]Untrusting ~ Technocratic/Geniocratic tendencies ~ Isolation-oriented ~ Socially aloof ~ [*]Dislikes manual labor ~ Over-reliant on technology ~ Optimistic "doomsday prepper" mindset ~ Scatterbrained ~ Rambler ~ [*]Dismissive of profit-folk and businesspeople's motives ~ Doesn't stay in-touch with friends/family ~

💫 Undefined / Other 💫

~ Neurodivergent(Schizotypal + others) ~ Bushcraft interest ~ Experimental archaeology interest ~ DIY interest ~ Arcology interest ~ Colon Classification system interest ~ Magical training systems interests ~

∴

## Political, Ethical, Economic leanings ##:
(Items in bold I am most interested and/or knowledgable in)

Before around 2020, I used to be quite the "apolitical", "neutral" type who did not properly study pre-existing systems. I assumed they're all broken, and while that's still possible, I'm blown away at the specifics of certain ones. It's implementation details and ethics that matter. I am still learning, but I'm so much more aware relativity, and this should give you a clue.

~ Leftism ~ Anarchism ~ Democratic Confederalism ~ Communalism ~ Mutualism ~ Libertarian Municipalism ~ Meta-Anarchism ~ Anarcho-Transhumanism ~ Anarcho-Communism ~ Fully Automated Semi-Luxury Queer Space Meta-Anarchism (I made this one up; do you like it?) ~

∴

## What do I really want to do in the near-future though? ##:

[ Game Programming & Development ]: Assets, modules, modular frameworks, software, templates, snippets, and so on; I don't need to make finished games, but I most definitely want to make these! I founded the GPD(Game Programming & Development) club in 2014, so making things for other game developers has always been my intent! I hope to nudge others to join in the quest of MD0 asset-making, or just find people who could use the creations - all without needing to officially "join my club" at all.

[ Remotely Assisting ]: If employment is kept at bay, I wouldn't mind assisting others in a slightly more directed approach, as long as it doesn't hinder the open creations(which many more people could make use of). Help-desk/tech-support is my typical job, I can help out in a few things. Strangely, a "consultant" of sorts might do more as I suggest interesting skills, tools, and technologies to empower them or their goals. 

[ Radical Armchair Slacktivism ]: The socio-spatial realms of physical proximity - in my opinion - have skillful individuals that are far more equipped and enthusiastic than me. In-person help is limited and local; I understand the need, but I am all for focusing on the global, internet-connected, virtual cyberscape of humankind. English and US-centric thinking do sadly interfere.
Radical? I always have been it seems, but I wasn't direct enough, nor knowledgable of the pre-existing definitions. Armchair? It is kind of "all talk", though. I find even that to be valuable, but I understand the downplay of calling it 'slack'-tivism. That's fine, I'll take it if you insist! I would like to be more "all code", or "all DIY", but I am also glad I could simmer and explore the intellectual wonderland, in which focused projects may have prevented.
I am, however, more ready than ever for grassroots participation in projects, plans, or even neighborhood preparedness. Still, in front of a computer - that's where I shine. Plenty to be done there, and honestly it is too-often an ignored or minimized tool. We need databases, interfaces, knowledge repositories, video hosts, simulations, workstations, and even drone controller seats - please, please don't just use manual labor all day and regress. I do not support anarcho-primitivism, nor a return to infinite agribusiness or serfdom - automation or bust, do not rally support heavily around in-person action and human strength and endurance.

Have you seen me ramble in walls of text, you'd get a glimpse at a few concepts I'm into. I'm not fully sure what I alone could do, but I sure would like to embrace some or all of these:

    ⚙ Deep technological and systemic change - Microfactories, microfabrication, teleoperations, telerobotics - don't leave tools out of the picture.
    ⎌ Abolish work, Disallow homeless exile - UBI ASAP, until funny money isn't needed anymore.
    ⎌ Abolish prisons, redesign the legal system and "legalese" - Flawed language and malpractice is torturing people
    ⭘ Communalistic Interdependence - In designing, planning, building an interdependence of vertical farms, food forests, water collection/storage/filtration, etc...
    ⏥ Flatten harmful(all?) hierarchies, end rulership - Hierarchies being dismantled and made obsolete
    ⏛ Plan the obsolescence of planned obsolescence - Lifetime Design should be prioritized.
    ⛮ Advanced "Unrealistic" & "Utopic" Technology - Autonomous space stations, self-replicating machines, nanobots, survival suits, etc.

The best way to understand me is to have heard(read?) me ramble in large, TL;DR paragraphs of technological and ethical solutions via ideacrafting. I hope to have an easy-to-browse "Digital Garden" and more content, someday.
# Whatcha using the money for, Nohbdy?
Rent + Bills (as of 2022-09, this is $950-1,100 per-month)

Yes, I'm transparent about bills. I'd be open about what I spend donations on too.
My free time to learn, play, and create "is" my investment. With employment out of the way, the "create" side will likely shine! I may not want to promise anything, but knowing that the act of giving has helped me to such a degree - I'm going to want to give back.

I am quite frugal. If bills are $1,100 then only $100 more feels adequate for food in a month, hence why $1,200 is my true "one month" donation option.
I really wish I did not have to spend this much on rent, even though some(most?) of you are perhaps surprised at the low price - I am quite lucky. I'd live in a $30-$100/month capsule hotel if I could. Even though housing should be free.

Work is torment, suffering, and soul-draining for me. Friends and family see me go through it seemingly unscathed, or even happily enjoying it... but no. I've told them it destroys me. Of course, I am still urged to continue on. I have done so, and I hate it. Help?

I am mentally preparing to return to work as we speak - I feel for any of you who are in a similar position. Donations to me will be helping my mental state greatly.
If donations surpass my need to continue employment for survival, though I'd like to not commit nor promise anything, I would happily provide more tangible, share-worthy creations that relate to my intests(as you've read them here).

There is no "job" that will ever allow me to provide my best for everyone as an individual, but I have an absurdly vast set of ideas on how to make an impact as an individual, within a small group, or alongside a communally mutualistic coalition. Doing so and/or curating knowledge how someone can do so themselves; yes, I can do that.
The knowledge itself will be public domain as well. Likely MD0, but certainly permissive.

I cannot commit to a project. I am quite committed to my ethics and goals, but there will be no contracts, promises, or so on. You'll have to take me at my word and see how closely I follow my intent. No worries. Just you wait and see what I can bring to the surface; perhaps for people even better equipped than I to build off of(very important, hence the .
Expect extrmeely transparent discussion on my fund expenditures, and an assortment of output that I hope you find worthwhile. I am quite sure I'll give more than any 'committed path' could allow me. Words are ambiguous, try to see my overall intent.

You understand the gist of me now. If you are struggling financially, hold tight - no need to donate, stay frugal my, friend, do what you can to reduce your expenses(including systemic change-making to stop these expenses in the first place). If you have more than you need, then I ask you kindly for assistance, in the chance that I will do the right thing with your contributions.

You won't be funding a profit-seeking fiend, I promise you that. In fact, I know you'd be hard-pressed to want to support someone like me if you are a status-quo-loving, pro-capitalistic, pro-work, "Dreams-of-being-an-Elite-themselves" kinda person(my goodness there are so many of them). I'm not going to be any nicer to you even if donate, but good thinking donating to something anti-proprietary. It still benefits you too, during your dismantling process. Keep making smart moves like that - donate more, or get to building/supporting non-business community stuff. The groups and the individuals are useful in different ways, for making a good world.
I can barely comprehend releasing a proprietary piece of anything. Hey, that means you can profit off my work, I suppose(though "Social Domain" and "Copyleft" licenses are not off the table in certain cases... we'll see).
I'm receptive to your suggestions, so please let me know(whether you donate or not).

However, if you expect closed-source, black-boxes to be tossed into the void, or into the hands of our planet-busting fiends: the profit-folk, the rich, and the bigoted(and boy do I have a lot to say about them...). Speaking of which...
Fight bigotry. Out-invent the unethical.
💚☯🔍⚛🔬⚗🧪⚒🧰🛠🧱🔭🗜💡🗝⚡☀🔋🖥

